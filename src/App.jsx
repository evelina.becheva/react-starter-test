import { hot } from "react-hot-loader/root";
import React, { Component } from "react";
import "./App.css";

class App extends Component {
  render() {
    const add = (a, b) => a + b;
    let result = add(4, 5);
    return (
      <div className="App">
        <h1>Hello, folks! {result}</h1>
      </div>
    );
  }
}

export default hot(App);
